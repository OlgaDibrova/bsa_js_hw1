function factorial(n) {
    return (n != 1) ? n * factorial(n - 2) : 1;
  }
  
  alert( factorial(1) );
  alert( factorial(2) );
  alert( factorial(9) );
  alert( factorial(0) );